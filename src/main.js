import Vue from 'vue'
import BootstrapVue from "bootstrap-vue"
import VueRouter from 'vue-router'
import App from './App.vue'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap-vue/dist/bootstrap-vue.css"
import router from './router';

Vue.use(BootstrapVue);
Vue.use(VueRouter);

new Vue({
    el: '#app',
    router,
    render: h => h(App)
});
