import Vue from 'vue';
import Router from 'vue-router';
import Simulate from '../components/Simulate.vue';
import Result from '../components/Result.vue';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/simulate',
            name: 'Simulate',
            component: Simulate,
        },
        {
            path: '/',
            name: 'Simulate',
            component: Simulate,
        },
        {
            path: '/result',
            name: 'Result',
            component: Result,
            props: true,
        },
    ],
});
